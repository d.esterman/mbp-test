package de.estermann.examples.mbp_test;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

public class DummyActivator implements BundleActivator
{

    @Override
    public void start(BundleContext context) throws Exception
    {
        System.out.println("blubb");
    }

    @Override
    public void stop(BundleContext context) throws Exception
    {}

}
