package de.estermann.examples.mbp_test;

public class DummyEntity
{
    private String dummyValue;

    public String getDummyValue()
    {
        return dummyValue;
    }

    public void setDummyValue(String dummyValue)
    {
        this.dummyValue = dummyValue;
    }
}
